//ARCHIVO EDITADO POR GONZALO ROJO SANTAMARÍA

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include<string>
#include<iostream>
#include <errno.h>
#include"Puntuaciones.h"
int main(){
int fd, error, lectura;
Puntuaciones puntos;
/* crea el FIFO */
	error = mkfifo("FIFO", 0600);
	if (errno != EEXIST && error < 0) {
	perror("No puede crearse el FIFO");
		return -1;
	}
	/* Abre el FIFO */
	if ((fd=open("FIFO", O_RDONLY))<0) {
		perror("No puede abrirse el FIFO");
		return -1;
	}
	
	error = 1;
	
	while (error > 0)  {
		lectura = read(fd, &puntos, sizeof(puntos))==sizeof(puntos);
		if(lectura < 0)
		{
			perror("No se puede leer el FIFO");
			return -1;	
		}
		else if ( lectura == 0)
		{
			error =-1;
		}
		
		if(puntos.lastWinner==1)
		{
			printf("Jugador 1 marca 1 punto, lleva un total de %d puntos.\n",puntos.jugador1);
		}
		else if(puntos.lastWinner==2)
		{
			printf("Jugador 2 marca 1 punto, lleva un total de %d puntos.\n",puntos.jugador2);
		}
		
	}
	close(fd);
	unlink("FIFO");
	return(0);
}
