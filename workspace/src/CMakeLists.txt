INCLUDE_DIRECTORIES("${PROJECT_INCLUDE_DIR}")

ADD_EXECUTABLE(logger logger.cpp ${COMMON_SRCS})

SET(COMMON_SRCS 
	Mundo.cpp 
	Esfera.cpp
	Plano.cpp
	Raqueta.cpp
	Vector2D.cpp)

				
ADD_EXECUTABLE(bot bot.cpp ${COMMON_SRCS})	
TARGET_LINK_LIBRARIES(bot glut GL GLU)		
ADD_EXECUTABLE(tenis tenis.cpp ${COMMON_SRCS})

TARGET_LINK_LIBRARIES(tenis glut GL GLU)
